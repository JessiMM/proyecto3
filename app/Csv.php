<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Csv extends Model
{
    //
    protected $table ='csvs';

    protected $fillable = [ 'email' ,'lname', 'fname','address','city','state','zip','phone','ip','datetime','source','gender','birth','deliveryverified','verify_date', 'smtpresponse' ];
}
