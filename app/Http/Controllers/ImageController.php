<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Jobs\ProcessImage;
use Validator;
use App\Image;

class ImageController extends Controller
{
    //
    public function index(Request $request){
        return view('upload_form');
    }

    public function upload(Request $request){
	 	// upload image
	    $this->validate($request, [
	        'demo_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	    ]);
	    $image = $request->file('demo_image');
	    $input['demo_image'] = time().'.'.$image->getClientOriginalExtension();
	    $destinationPath = public_path('/images');
	    $image->move($destinationPath, $input['demo_image']);
	 
	    // ingresar la imgen a la BD
	    $image = new Image;
	    $image->org_path = 'images' . DIRECTORY_SEPARATOR . $input['demo_image'];
	    $image->save();
	 
	    // Procesar la imagen con el JOB
	   	dispatch(new ProcessImage($image));
	   	  
	    return Redirect::to('image/index')->with('message', 'Imagen cargada exitosamente!');
    }

}
