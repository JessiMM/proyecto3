<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Jobs\ImportCSV;
use App\Jobs\GenerateXML;
use App\Archivo;
use App\Csv;
use Validator;

class ArchivoController extends Controller
{
    //
    public function index(Request $request){
        return view('archivo');
    }

    public function upload(Request $request){
    	//amplia memoria y procesa mas registros 
    	ini_set('memory_limit','1024M');
	 	// upload file
	    $this->validate($request, [
	        'demo_file' => 'required|file|mimes:csv,txt',
	    ]);
	    $files = $request->file('demo_file');
	    $input['demo_file'] = time().'.'.$files->getClientOriginalExtension();
	    $destinationPath = base_path('/database/seeds/csv');
	    $files->move($destinationPath, $input['demo_file']);
	 
	    // ingresar archivo  a la BD
	    $files = new Archivo;
	    $files ->org_path = 'database\seeds\csv' . DIRECTORY_SEPARATOR . $input['demo_file'];
	    $files ->save();
	 
	    // Procesa archivo con el JOB	   
	   	dispatch(new ImportCSV($files));	

	   	$datos = Csv::all();

	    // Genera XML con el job   
	    dispatch(new GenerateXML($datos));    	 
	 
	    return Redirect::to('archivo/index')->with('message', 'Archivo cargado exitosamente!');
    }
}
