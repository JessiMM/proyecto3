<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Keboola\Csv\CsvFile;
use App\Archivo;
use App\Csv; 

class ImportCSV implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $files;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Archivo $files)
    {
        //
         $this->files = $files;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $files= $this->files;
        $csv= new CsvFile(base_path().DIRECTORY_SEPARATOR.$files->org_path ,  ',');
        foreach($csv AS $row) {
              $inserta= new Csv;
              $inserta->create([
                'email'=> $row[0],
                'fname'=> $row[1],
                'lname'=> $row[2],
                'address'=> $row[3],
                'city'=> $row[4],
                'state'=> $row[5],
                'zip'=> $row[6],
                'phone'=> $row[7],
                'ip'=> $row[8],
                'datetime'=> $row[9],
                'source'=>$row[10],
                'gender'=> $row[11],
                'birth'=> $row[12],
                // 'deliveryverified'=> $row[13], 
                'verify_date'=> $row[14],
                'smtpresponse'=> $row[15]
              ]);
        };  

        \Log::info('Archivo Enviado');
    }
}
