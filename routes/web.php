<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	//rutas para subir imagenes
	Route::get('image/index', 'ImageController@index')->name('imagen');
	Route::post('image/upload', 'ImageController@upload');

	//rutas para subir archivo
	Route::get('archivo/index', 'ArchivoController@index')->name('archivo');
	Route::post('archivo/upload', 'ArchivoController@upload');

});

Route::fallback(function() {
    return abort(404);
});